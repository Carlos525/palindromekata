package mx.tec.lab;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PalindromeVerifierTests {

	@Test
	public void givenASingleCharacter_WhenVerify_ThenTrue() {
		// Given
		String input = "a";
		boolean expectedResult = true;

		// When
		boolean result = PalindromeVerifier.verify(input);

		// Then
		assertEquals(expectedResult, result);
	}

	@Test
	public void givenTwoDifferentCharacters_WhenVerify_ThenFalse() {
		// Given
		String input = "ab";
		boolean expectedResult = false;

		// When
		boolean result = PalindromeVerifier.verify(input);

		// Then
		assertEquals(expectedResult, result);
	}

	@Test
	public void givenTwoEqualCharacters_WhenVerify_ThenTrue() {
		// Given
		String input = "aa";
		boolean expectedResult = true;

		// When
		boolean result = PalindromeVerifier.verify(input);

		// Then
		assertEquals(expectedResult, result);
	}

	@Test
	public void givenThreeLetterWordPalindrome_WhenVerify_thenTrue() {
		// Given
		String input = "aba";
		boolean expectedResult = true;

		// When
		boolean result = PalindromeVerifier.verify(input);

		// Then
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void givenASentencePalindrome_WhenVerify_thenTrue() {
		// Given
		String input = "a man a plan a canal panama";
		boolean expectedResult = true;

		// When
		boolean result = PalindromeVerifier.verify(input);

		// Then
		assertEquals(expectedResult, result);
	}
}
